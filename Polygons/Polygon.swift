//
//  Polygon.swift
//  Polygons
//
//  Created by Kamal Dandamudi (z1762755) & Pravin Kandala (z1761257)
//  Copyright (c) 2015. All rights reserved.
//

import Foundation

class Polygon{
    //Properties of the Polygon class
    var minSides: Int = 3
    var maxSides: Int = 12
    var sides: Int
    var name: String {
        //Computed Property for name of Polygon
        get{
            switch sides {
            case 3:
                return "Triangle"
            case 4:
                return "Square"
            case 5:
                return "Pentagon"
            case 6:
                return "Hexagon"
            case 7:
                return "Heptagon"
            case 8:
                return "Octagon"
            case 9:
                return "Nonagon"
            case 10:
                return "Decagon"
            case 11:
                return "Hendecagon"
            default:
                return "Dodecagon"
            }
        }
    }
    //Designated Initializer for Polygon class
    init(minSides:Int,maxSides:Int,sides:Int){
        self.minSides=minSides
        self.maxSides=maxSides
        self.sides=sides
    }
    //Convenience Initializer for Polygon class
    convenience init(){
        self.init(minSides: 3, maxSides: 12, sides: 5)
    }
    //Method for Interior Angles in Degrees for Polygon
    func interiorAngleInDegree()->Double{
        return 180 * Double(sides-2)/Double(sides)
    }
    //Method for Interior Angles in Radian for Polygon
    func interiorAnglesInRadians()->Double{
        return self.interiorAngleInDegree() * (M_PI/180)
    }
    //Method for description of Polygon
    func description()->String{
        return  "I am a \(sides)-sided polygon (a.k.a a \(name)) with interior angles of \(interiorAngleInDegree()) degrees (\(interiorAnglesInRadians()) radians)\n"
    }
}