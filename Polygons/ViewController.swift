//
//  ViewController.swift
//  Polygons
//
//  Created by Kamal Dandamudi (z1762755) & Pravin Kandala (z1761257)
//  Copyright (c) 2015. All rights reserved.
//

import UIKit

//Global Constant for Step Value
let STEP_VALUE:Double=1

class ViewController: UIViewController {
    //Outlets for changing labels and stepper
    @IBOutlet weak var sidesLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sideStepper: UIStepper!
    
    //Creating Instance of the Polygon Class
    let polygon=Polygon()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting Labels for Polygon sides,Polygon name and printing Polygon description on Load of View
        sidesLabel.text=String(polygon.sides)
        nameLabel.text=polygon.name
        println(polygon.description())
        
        //Setting min, max and value for sideStepper on Load of View
        sideStepper.minimumValue=Double(polygon.minSides)
        sideStepper.maximumValue=Double(polygon.maxSides)
        sideStepper.value=Double(polygon.sides)
        sideStepper.stepValue=STEP_VALUE
        
    }
    
    //Event function for Stepper to update Label values
    @IBAction func sideStepperValueChanged(sender: AnyObject) {
        polygon.sides=Int(sideStepper.value)
        sidesLabel.text=String(polygon.sides)
        nameLabel.text=polygon.name
        println(polygon.description())
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}